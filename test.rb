require "pry"
require "json"
require "sequel"
require "mysql2"

DB = Sequel.connect("mysql2://root@localhost/flight_scraper")

prices = JSON.parse(open("prices.json").read)

prices.each do |o, data|
  data.each do |d, flight_dates|
    flight_dates.each do |date, price|
      puts "#{o} - #{d} - #{date} - #{price}"
      DB[:flights].insert({
        origin: o,
        destination: d,
        flight_date: date,
        price: price,
        duration: 5,
        updated_at: Time.now
      })
    end
  end
end