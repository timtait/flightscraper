require "pry"
require "nokogiri"
require "watir"
require "csv"

module Watir
  module Container
    def calendar_day(*args)
      CalendarDay.new(self, extract_selector(args).merge(:tag_name => "calendar-day"))
    end

    def calendar_days(*args)
      CalendarDayCollection.new(self, extract_selector(args).merge(:tag_name => "calendar-day"))
    end
  end

  class CalendarDay < Element
  end

  class CalendarDayCollection < ElementCollection
    def element_class
      CalendarDay
    end
  end
end

SEARCH_2_MONTHS = 6
DURATION_DAYS = 5

origins = %w{ DUD WLG }
destinations = []
destinations = %w{ SYD MEL BNE NSN NPE ZQN CHC }
start_date = Date.today
end_date = start_date + DURATION_DAYS
destinations = (destinations + origins).flatten
@prices = {}


@b = Watir::Browser.new :chrome #, headless: true


# @prices = {
#   origin => {
#     destinations => {
#       day => price
#     }
#   }
# }

origins.each_with_index do |origin, oi|
  destinations.each_with_index  do |destination, di|
    next if origin == destination
    puts "#{origin} -> #{destination}"

    url = "https://www.google.com/flights#flt=#{origin}.#{destination}.#{start_date.to_s}*#{destination}.#{origin}.#{end_date.to_s};c:NZD;e:1;px:2,,1;sd:1;t:f"
    puts url
    @b.goto(url)
    @b.spans(css: ".gws-flights-form__date-content").first.click

    SEARCH_2_MONTHS.times do |month|
      remaining_tries = 3
      while remaining_tries > 0
        searched_dates = [start_date >> (month*2), start_date >> 1+(month*2)]
        @prices[origin] ||= {}
        @prices[origin][destination] ||= {}

        js_query = <<-JSQUERY
          days = document.querySelectorAll("calendar-day[data-day^='#{searched_dates[0].to_s[0..6]}'],calendar-day[data-day^='#{searched_dates[1].to_s[0..6]}']");
          data = {}
          for (i = 0; i < days.length; i++) {
            day = days[i].getAttribute("data-day");
            price = days[i].lastChild.innerText;
            data[day] = price
          }
          json = Object.assign({}, data);
          return JSON.stringify(json);
        JSQUERY
        days = JSON.parse(@b.execute_script(js_query))
        days.reject!{|k,v| v == ""}
        puts "found #{days.size} days"
        if days.size < 30
          puts "... retrying ..."
          remaining_tries -= 1
          sleep 5
        else
          break
        end
      end
      # days = @b.calendar_days.select do |day|
      #   day.attribute("data-day").start_with?() ||
      #     day.attribute("data-day").start_with?(searched_dates[1].to_s[0..6])
      # end

      # puts days.map do |day|
      #   flight_date = day.attribute("data-day")
      #   price = day.spans.first.text.strip.gsub("$", "").gsub(",", "")
      #   [flight_date, price]
      # end

      # days.each do |day|
      #   flight_date = day.attribute("data-day")
      #   price = day.spans.first.text.strip.gsub("$", "").gsub(",", "")
      #   puts "#{flight_date} - #{price}"
      #   # @prices[origin][destination][flight_date] = price unless price == ""
      # end

      days.each do |day, price|
        price = price.gsub("$", "").gsub(",", "").strip
        puts "#{day} - #{price}"
        @prices[origin][destination][day] = price unless price == ""
      end

      @b.divs(css: ".gws-travel-calendar__next").first.fire_event :click
      @b.divs(css: ".gws-travel-calendar__next").first.fire_event :click

      puts @prices
    end
  end
end

File.open("prices.json", "w") { |file| file.write(@prices.to_json)}
vue_json = prices.map{|o,dv| dv.map{|d, dayprices| {"#{o}_#{d}" => dayprices}}}.flatten
File.open("prices.json", "w") { |file| file.write(vue_json.to_json)}

rows = []
# date, o1_d1, o2_d1, o1_d2, o2_d2, ..
headers = destinations.map{|d| origins.map{|o| "#{o}-#{d}" unless o == d}}.flatten
headers.prepend("Date")
headers.compact!
i = Date.today+1
max_date = Date.today >> (SEARCH_2_MONTHS*2)-1
while i < max_date
  row = [i.to_s]
  destinations.each do |destination|
    origins.each do |origin|
      next if origin == destination
      row << @prices[origin][destination][i.to_s]
    end
  end
  rows << row
  i += 1
end

# origins.each do |origin|
#   (origins-[origin]).each do |destination|
#     headers << "#{origin}-#{destination}"
#     i = Date.today+1
#     max_date = Date.today >> (SEARCH_2_MONTHS*2)-1
#     while i < max_date
#       row[i] << @prices[origin][destination][i.to_s]
#       i += 1
#     end
#   end
# end

CSV.open("flights.csv", "w") do |csv|
  csv << headers
  rows.each do |row|
    csv << row
  end
end
binding.pry

colors_css = ""
101.times do |i|
  r = 255.0 * (i.to_f / 100.0)
  g = 255.0 - (255.0 * (i.to_f / 100.0))
  colors_css += ".c_#{i} { background-color: rgb(#{r}, #{g}, 0); }\n"
end

# col_values = []
# (1..rows.first.size).each_slice(2).to_a.each do |pair|
#   prices = (rows.map{|x| x[pair[0]].to_i} + rows.map{|x| x[pair[1]].to_i}) - [0]
#   col_values[pair[0]] = {min: prices.min, max: prices.max, diff: prices.max-prices.min}
# end

# prices = (rows.map{|x| x[1].to_i} + rows.map{|x| x[2].to_i}) - [0]
# min = prices.min
# max = prices.max

col_values = []
(1..(rows.first.size-1)).each do |col|
  puts col
  prices = rows.map{|row| row[col].to_i} - [0]
  puts prices

  col_values[col] = {min: prices.min, max: prices.max, diff: prices.max-prices.min}
end

html_rows = ""
rows.each do |row|
  html_rows += "<tr>"
  row.each_with_index do |price, i|
    price_range = col_values[i]
    if price_range.nil? || price == ""
      html_rows += "<td>#{price}</td>"
    else
      price_f = price.to_f
      price_f = (price_f - price_range[:min]) / price_range[:diff]
      html_rows += "<td class='c_#{(price_f*100.0).round}'>#{price}</td>"
    end
  end
  html_rows += "</tr>\n"
end

# html_rows = rows.map{|row| row.map{|x| "<td >#{x}</td>"}.join("")}
html = <<-HTML
<head>
<style>
#{colors_css}
table{border-collapse:collapse;text-align:center;}
td{background: #fff;border-bottom:1px solid #333;}
th:nth-child(4n),td:nth-child(4n) {border-left: 1px solid #333;}
th:nth-child(4n+1),td:nth-child(4n+1) {border-right: 1px solid #333;}
</style>
</head>
<body>
<table>
  <tr>#{headers.map{|x| "<th>#{x}</th>"}.join("")}</tr>
  #{html_rows}
</table>
</body>
HTML
File.open("flights.html", "w") { |file| file.write(html)}