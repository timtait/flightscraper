require "pry"
require "nokogiri"
require "watir"
require "uri"
require "webdrivers/geckodriver"

airports = {
  "ZQN" => "Queenstown",
  "DUD" => "Dunedin",
  "CHC" => "Christchruch",
  "AKL" => "Auckland",
  "SYD" => "Sydney",
  "MEL" => "Melbourne",
  "BNE" => "Brisbane"
}

origins = %w{ DUD WLG } #CHC ZQN AKL
destinations = %w{ CHC AKL } #CHC AKL

@b = Watir::Browser.new :firefox #, headless: true
at_exit do
  @b.close
end

html = <<-HTML
<head>
<style>
* {font-family:verdana;}
a {text-decoration:none;}
a:visited {color:blue;}
h1 {font-size: 1.5em;font-weight: normal;}
h3 {font-weight:normal;font-size:1.2em;}
.flights_dates {background: #c2d9ec;padding:1px 10px 10px 10px;margin-bottom:20px;}
.flights {background: #fffafa;padding:1px;}
.city_name {width:110px;display:inline-block;}
</style>
</head>
<body>\n
HTML

dates = []
start_date = Date.parse("2019-09-27")
(1..10).each do |i|
  dates << [start_date+(i*7), start_date+(i*7)+2]
end

# adults = 1
# under2 = 1
# kids = 2
# "px:#{adults},#{kids},#{under2}" # double check order of kids

dates.each do |flight_dates|
  start_date, end_date = flight_dates
  html << "<div class='flights_dates'><h1>#{start_date.strftime('%a %d %b')} > #{end_date.strftime('%a %d %b')}</h1>"
  origins.each_with_index do |origin, oi|
    html << "<div class='flights'><h3>#{airports[origin]}</h3><ul>"
    destinations.each_with_index  do |destination, di|
      next if origin == destination

      url = "https://www.google.com/flights#flt=#{origin}.#{destination}.#{start_date.to_s}*#{destination}.#{origin}.#{end_date.to_s};c:NZD;e:1;px:1;sd:1;t:f"
      puts "#{origin}>#{destination} - #{url}"
      @b.goto(url)
      sleep 10

      prices = @b.divs(css: ".gws-flights-results__price").map{|x|x.text.gsub("$","").gsub(",","").to_i}.select{|x| x > 0}.sort.take(5).join(", ")
      puts prices
      html << "<li><a href='#{url}'><span class='city_name'>#{airports[destination]}</span> - #{prices}</a></li>"
      puts "- - - - - - - - - - - - -"
    end
    html << "</ul></div>\n"
  end
  html << "</div>"
end

@b.close

html << "Updated at: #{Time.now.strftime("%c")}</body>"

File.open("flights-nz.html", "w") { |file| file.write(html)}
